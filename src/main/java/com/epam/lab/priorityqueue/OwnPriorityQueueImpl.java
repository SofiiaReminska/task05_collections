package com.epam.lab.priorityqueue;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Own implementation of PriorityQueue.
 */
public class OwnPriorityQueueImpl<E extends Comparable> extends AbstractQueue<E> {
    private Object[] queue;
    private int size = 0;
    private static final int LENGTH = 11;

    public OwnPriorityQueueImpl() {
        this(LENGTH);
    }

    public OwnPriorityQueueImpl(int capacity) {
        this.queue = new Object[capacity];
    }

    @Override
    public Iterator<E> iterator() {
        throw new NotImplementedException();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean offer(E e) {
        growIfNeed();
        queue[size] = e;
        size++;
        Arrays.sort(queue, 0, size);
        return true;
    }

    private void growIfNeed() {
        if (queue.length == size) {
            queue = Arrays.copyOf(queue, (int) (queue.length * 1.5));
        }
    }

    @Override
    public E poll() {
        if (size == 0)
            return null;
        --size;
        E result = (E) queue[0];
        queue[0] = null;
        queue = Arrays.copyOfRange(queue, 1, queue.length - 1);
        return result;
    }

    @Override
    public E peek() {
        return (size == 0) ? null : (E) queue[0];
    }
}
