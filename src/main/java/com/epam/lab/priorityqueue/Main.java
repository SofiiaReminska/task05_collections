package com.epam.lab.priorityqueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {

    static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        OwnPriorityQueueImpl<Integer> queue = new OwnPriorityQueueImpl<>();
        queue.offer(2);
        queue.offer(5);
        queue.add(1);
        queue.offer(7);
        LOGGER.info("Polled element is " + queue.poll());
        LOGGER.info("Offered element 3 " + queue.offer(3));
        while (!queue.isEmpty()) {
            LOGGER.info("Removed element " + queue.remove());
        }
    }
}
