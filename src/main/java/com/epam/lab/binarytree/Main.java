package com.epam.lab.binarytree;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    static final Logger LOGGER = LogManager.getLogger(Main.class);

    /**
     * Entry point for testing methods of OwnBinaryTreeImpl class.
     *
     * @param args - entry parameters.
     */
    public static void main(String[] args) {
        OwnBinaryTreeImpl<Integer> tree = new OwnBinaryTreeImpl<>();
        tree.addNode(50);
        tree.addNode(25);
        tree.addNode(15);
        tree.addNode(30);
        tree.addNode(75);
        tree.addNode(85);
        LOGGER.info("Preorder traversal tree representation:");
        LOGGER.info(tree.print());
        LOGGER.info(tree.findNode(25));
    }
}
