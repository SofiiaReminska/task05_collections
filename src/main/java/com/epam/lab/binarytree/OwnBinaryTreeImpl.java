package com.epam.lab.binarytree;

import java.util.Objects;

class Node<T extends Comparable<T>> {
    T value;
    Node<T> rightChild;
    Node<T> leftChild;

    Node(T value) {
        this.value = value;
    }

    public String toString() {
        return value.toString();
    }
}

public class OwnBinaryTreeImpl<T extends Comparable<T>> {
    public Node<T> root;

    /**
     * Method for adding new node to the tree.
     *
     * @param value - value of the node.
     */
    public void addNode(T value) {
        Node<T> newNode = new Node<>(value);
        if (Objects.isNull(root)) {
            root = newNode;
        } else {
            Node<T> focusNode = root;
            Node<T> parent;
            while (true) {
                parent = focusNode;
                if (value.compareTo(focusNode.value) < 0) {
                    focusNode = focusNode.leftChild;
                    if (Objects.isNull(focusNode)) {
                        parent.leftChild = newNode;
                        return;
                    }
                } else {
                    focusNode = focusNode.rightChild;
                    if (Objects.isNull(focusNode)) {
                        parent.rightChild = newNode;
                        return;
                    }
                }
            }
        }
    }

    /**
     * Method for searching nodes.
     *
     * @param value - value of the node.
     * @return -
     */
    public Node findNode(T value) {
        Node<T> focusNode = root;
        while (!focusNode.value.equals(value)) {
            if (value.compareTo(focusNode.value) < 0) {
                focusNode = focusNode.leftChild;
            } else {
                focusNode = focusNode.rightChild;
            }
            if (Objects.isNull(focusNode)) {
                return null;
            }
        }
        return focusNode;
    }

    /**
     * Method for removing node of a tree.
     *
     * @param value - value of the node.
     * @return - true, if node was removed. false if was not.
     */
    public boolean remove(T value) {
        Node<T> focusNode = root;
        Node<T> parent = root;
        boolean isALeftChild = true;
        while (!focusNode.value.equals(value)) {
            parent = focusNode;
            if (value.compareTo(focusNode.value) < 0) {
                isALeftChild = true;
                focusNode = focusNode.leftChild;
            } else {
                isALeftChild = false;
                focusNode = focusNode.rightChild;
            }
            if (Objects.isNull(focusNode)) {
                return false;
            }
        }
        if (Objects.isNull(focusNode.leftChild) && Objects.isNull(focusNode.rightChild)) {
            if (focusNode == root) {
                root = null;
            } else if (isALeftChild) {
                parent.leftChild = null;
            } else {
                parent.rightChild = null;
            }
        } else if (Objects.isNull(focusNode.rightChild)) {
            if (focusNode == root) {
                root = focusNode.leftChild;
            } else if (isALeftChild) {
                parent.leftChild = focusNode.leftChild;
            } else {
                parent.rightChild = focusNode.leftChild;
            }
        } else if (Objects.isNull(focusNode.leftChild)) {
            if (focusNode == root) {
                root = focusNode.rightChild;
            } else if (isALeftChild) {
                parent.leftChild = focusNode.rightChild;
            } else {
                parent.rightChild = focusNode.leftChild;
            }
        } else {
            Node<T> replacement = getReplacementNode(focusNode);
            if (focusNode == root) {
                root = replacement;
            } else if (isALeftChild) {
                parent.leftChild = replacement;

            } else {
                parent.rightChild = replacement;
            }
            replacement.leftChild = focusNode.leftChild;
        }
        return true;
    }

    /**
     * Method for getting the replacement node.
     * Is used by method remove.
     *
     * @param replacedNode - object of type Node<T>.
     * @return - replacement of the node.
     */
    private Node<T> getReplacementNode(Node<T> replacedNode) {
        Node<T> replacementParent = replacedNode;
        Node<T> replacement = replacedNode;
        Node<T> focusNode = replacedNode.rightChild;
        while (!Objects.isNull(focusNode)) {
            replacementParent = replacement;
            replacement = focusNode;
            focusNode = focusNode.leftChild;
        }
        if (!replacement.equals(replacedNode.rightChild)) {
            replacementParent.leftChild = replacement.rightChild;
            replacement.rightChild = replacedNode.rightChild;
        }
        return replacement;
    }

    /**
     * Method for printing binary tree as a string.
     *
     * @return - binary tree as a string.
     */
    public String print() {
        return print(root, new StringBuilder());
    }

    /**
     * Method for converting binary tree to a string.
     *
     * @param node          - node of the binary tree.
     * @param stringBuilder - object of type StringBuilder.
     */
    private String print(Node<T> node, StringBuilder stringBuilder) {
        if (Objects.isNull(node)) {
            return stringBuilder.toString();
        }
        stringBuilder.append(node).append(" ");
        print(node.leftChild, stringBuilder);
        print(node.rightChild, stringBuilder);
        return stringBuilder.toString();
    }
}
