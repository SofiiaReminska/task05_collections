package com.epam.lab.dequeue;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

public class OwnDequeImpl<E> extends AbstractCollection<E> implements Deque<E> {

    private class Node {
        Node previous;
        Node next;
        E value;

        public Node() {
        }

        public Node(Node previous, Node next, E value) {
            this.previous = previous;
            this.next = next;
            this.value = value;
        }
    }

    private Node head;
    private Node tail;
    private int size = 0;

    public OwnDequeImpl() {
    }

    @Override
    public Iterator<E> iterator() {
        throw new NotImplementedException();
    }

    @Override
    public Iterator<E> descendingIterator() {
        throw new NotImplementedException();
    }

    @Override
    public void addFirst(E e) {
        if (Objects.isNull(e))
            throw new NullPointerException();
        final Node h = head;
        final Node newNode = new Node(null, h, e);
        tail = newNode;
        if (Objects.isNull(h))
            head = newNode;
        else
            h.next = newNode;
        size++;
    }

    @Override
    public void addLast(E e) {
        if (Objects.isNull(e))
            throw new NullPointerException();
        final Node t = tail;
        final Node newNode = new Node(t, null, e);
        tail = newNode;
        if (Objects.isNull(t))
            head = newNode;
        else
            t.next = newNode;
        size++;
    }

    @Override
    public boolean offerFirst(E e) {
        addFirst(e);
        return true;
    }

    @Override
    public boolean offerLast(E e) {
        addLast(e);
        return true;
    }

    @Override
    public E removeFirst() {
        E x = pollFirst();
        if (Objects.isNull(x))
            throw new NoSuchElementException();
        return x;
    }

    @Override
    public E removeLast() {
        E x = pollLast();
        if (Objects.isNull(x))
            throw new NoSuchElementException();
        return x;
    }

    @Override
    public E pollFirst() {
        if (size <= 0)
            throw new IllegalStateException();
        E result = head.value;
        remove(head);
        return result;
    }

    @Override
    public E pollLast() {
        if (size <= 0)
            throw new IllegalStateException();
        E result = tail.value;
        remove(tail);
        return result;
    }

    @Override
    public E getFirst() {
        if (size <= 0)
            throw new IllegalStateException();
        return head.value;
    }

    @Override
    public E getLast() {
        if (size <= 0)
            throw new IllegalStateException();
        return tail.value;
    }

    @Override
    public E peekFirst() {
        return head.value;
    }

    @Override
    public E peekLast() {
        return tail.value;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        if (size <= 0)
            throw new IllegalStateException();
        Node tmp = head;
        while (!Objects.isNull(tmp)) {
            if (tmp.value.equals(o)) {
                remove(tmp);
                return true;
            }
            tmp = tmp.next;
        }
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        if (size <= 0)
            throw new IllegalStateException();
        Node tmp = tail;
        while (!Objects.isNull(tmp)) {
            if (tmp.value.equals(o)) {
                remove(tmp);
                return true;
            }
            tmp = tmp.previous;
        }
        return false;
    }

    @Override
    public boolean offer(E e) {
        return offerLast(e);
    }

    @Override
    public E remove() {
        return removeFirst();
    }

    @Override
    public E poll() {
        return pollFirst();
    }

    @Override
    public E element() {
        return getFirst();
    }

    @Override
    public E peek() {
        return peekFirst();
    }

    @Override
    public void push(E e) {
        addFirst(e);
    }

    @Override
    public E pop() {
        return removeFirst();
    }

    @Override
    public int size() {
        return size;
    }

    private void remove(Node node) {
        if (!Objects.isNull(node.previous)) {
            node.previous.next = node.next;
        } else if (head != tail) {
            head = node.next;
        } else {
            head = tail = null;
        }

        if (!Objects.isNull(node.next)) {
            node.next.previous = node.previous;
        } else if (head != tail) {
            tail = node.previous;
        } else {
            head = tail = null;
        }
        size--;
    }
}
