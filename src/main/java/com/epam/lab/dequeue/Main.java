package com.epam.lab.dequeue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Main {
    static final Logger LOGGER = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        OwnDequeImpl<Integer> d = new OwnDequeImpl<>();
        d.offer(4);
        d.offer(3);
        d.offer(2);
        d.offer(1);
        LOGGER.info("Polled " + d.poll());
        LOGGER.info("Polled " + d.poll());
        LOGGER.info("Polled " + d.poll());
        LOGGER.info("Polled " + d.poll());
    }
}
